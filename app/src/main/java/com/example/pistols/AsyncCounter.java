package com.example.pistols;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class AsyncCounter extends AsyncTask<ImageView, Byte, ImageView> {


    @Override
    protected ImageView doInBackground(ImageView... imageViews) {
        byte counter = 0;
        while(counter < MainActivity.STEPS_TO_COUNT){
            if(isCancelled()){return imageViews[0];}
            try {
                Thread.sleep(1000);
            }catch(InterruptedException e){
                Log.d(AsyncCounter.class.getSimpleName(),
                        "Se interrumpió el contador");
                return imageViews[0];
            }
            counter++;
            publishProgress(counter);
        }
        return imageViews[0];
    }

    @Override
    protected void onProgressUpdate(Byte... values) {
        Log.d(AsyncCounter.class.getSimpleName(),
                "Counted to: " + values[0]);
    }

    @Override
    protected void onCancelled(ImageView image) {
        if(image != null)
            image.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPostExecute(ImageView image) {
        image.setVisibility(View.VISIBLE);
    }
}
