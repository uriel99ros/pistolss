package com.example.pistols;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView gunView;
    @Nullable
    private SensorManager sensorManager;
    @Nullable
    private Sensor stepDetectorSensor;
    private byte steps; //Pasos
    public static final byte STEPS_TO_COUNT = 3;
    private AsyncCounter asyncCounter;
    private Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = findViewById(R.id.start_button);
        gunView = findViewById(R.id.gun_iv);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if(stepDetectorSensor == null){sensorManager = null;}
    }

    @Override
    protected void onPause() {
        killCounter();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
    }

    public void finalCowntdown(View startButton){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        startButton.setVisibility(View.INVISIBLE);
        checkStepSensor();
    }

    public void checkStepSensor(){
        if(sensorManager == null) {
            startTimer();
            return;
        }
        sensorManager.registerListener(this,
                stepDetectorSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void fire(View view){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SoundPlayer.enqueueWork(this, new Intent(SoundPlayer.ACTION_FIRE));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run(){
                init();
            }
        }, 3000);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        steps++;
        if(steps >= STEPS_TO_COUNT) {
            sensorManager.unregisterListener(this);
            gunView.setVisibility(View.VISIBLE);
            steps = 0;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public void startTimer(){
        if(asyncCounter != null &&
                !asyncCounter.getStatus().equals(
                        AsyncTask.Status.FINISHED)) {
            asyncCounter.cancel(true);
        }
        asyncCounter = new AsyncCounter();
        asyncCounter.execute(gunView);
    }

    private void init(){
        startButton.setVisibility(View.VISIBLE);
        gunView.setVisibility(View.INVISIBLE);
    }

    private void killCounter(){
        if(sensorManager != null){
            sensorManager.unregisterListener(this);
        }else if(asyncCounter != null &&
                !asyncCounter.getStatus().equals(AsyncTask.Status.FINISHED)){
            asyncCounter.cancel(true);
        }
    }
}